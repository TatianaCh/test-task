import xlsxwriter
import os
from io import BytesIO
from celery.task import task
from django.http import HttpResponse

from clients.models import Persons
from clients.utils import make_clients_dict

@task
def make_excel():
    info_list = Persons.objects.all().values()
    clients_info = make_clients_dict(info_list, 'date_of_birth');
    field_list = clients_info['field_list']
    client_list = clients_info['client_list']

    output = BytesIO()
    w_book = xlsxwriter.Workbook(output, {'in_memory': True})
    w_sheet = w_book.add_worksheet(Persons._meta.verbose_name)
    head_format = w_book.add_format({'underline': 1, 'border': 2})
    if len(client_list):
        length = []
        i = 0
        col_length = 0
        for fld in field_list.keys():
            w_sheet.write(0, i, field_list[fld], head_format)
            col_length = len(field_list[fld])
            for j in range(len(client_list)):
                w_sheet.write(j + 1, i, client_list[j][fld])
                if len(str(client_list[j][fld])) > col_length:
                    col_length = len(str(client_list[j][fld]))

            length.append(col_length)
            i = i + 1

        i = 0
        for i in range(len(field_list.keys())):
            w_sheet.set_column(i, i, length[i] + 2)
    else:
        w_sheet.write(0, 0, u"Не найдено записей в базе данных", head_format)
        
    w_book.close()
    output.seek(0)
    file_name = 'persons.xlsx'
    response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    response['Content-Disposition'] = "attachment; filename=" + file_name

    return response

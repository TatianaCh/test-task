# -*- coding: utf-8 -*-
import json
import datetime
import os

from django.shortcuts import render
from django.http import HttpResponse
from django.template.context_processors import csrf
from django.db import connection

from clientdb.settings import MEDIA_URL, MEDIA_ROOT, TASK_PATH
from clients.tasks import make_excel
from clients.utils import make_clients_dict
from clients.models import Persons, PersonsForm

def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]

def export_to_xls(request):
    return make_excel.delay().get()

def init(request):
    myform = PersonsForm()
    context = {'form': myform, "MEDIA_URL": MEDIA_URL}
    context.update(csrf(request))
    return render(request, 'catalog.html', context)

def get_clients(request):
    # request.get could be dictionary like that
    # {'order_by': '..', 'filter_f': '...', 'filter_v': '...'}

    info_list = Persons.objects.all()
    birth_field = 'date_of_birth'

    if request.GET:
        if 'filter_v' in request.GET and len(request.GET['filter_v']):
            lookup = "%s__icontains" % request.GET['filter_f']
            info_list = info_list.filter(**{ lookup: request.GET['filter_v']})

        if 'order_by' in request.GET:
            if request.GET['order_by'].find('-age') != -1:
                info_list = info_list.order_by(birth_field)
            elif request.GET['order_by'].find('age') != -1:
                info_list = info_list.order_by('-' + birth_field)
            else:
                info_list = info_list.order_by(request.GET['order_by'])

    info_list = info_list.values()

    context = make_clients_dict(info_list, birth_field)
    return HttpResponse(json.dumps(context), content_type="application/json")


def delete_person(request):
    statusCode = 200
    if request.GET:
        if 'ids' in request.GET:
            delete_ids = request.GET['ids'].split(',');
            persons = Persons.objects.filter(id__in=delete_ids).values()
            for row in persons:
                filename = MEDIA_ROOT + row['photo']
                try:
                    os.remove(filename)
                except OSError:
                    pass

            Persons.objects.filter(id__in=delete_ids).delete()

            message = 'ok'
        else:
            statusCode = 400
            message = 'error - no ids'
    else:
        statusCode = 400
        message = 'error - no GET values'
    return HttpResponse(message, status=statusCode)

def save_person(request):
    import time
    
    message = 'no files'

    if len(request.FILES) != 0:
        response = {}
        upload_image = request.FILES.getlist('photo')[0]
        #try:
        file_name = '%s' % (str(time.time()).replace('.', '')) + upload_image.name
        if not os.path.exists(MEDIA_ROOT):
            os.makedirs(MEDIA_ROOT)
        destination = open(MEDIA_ROOT + file_name, 'wb+')
        for chunk in upload_image.chunks():
            destination.write(chunk)
        destination.close()

        new_item = Persons()
        new_item.first_name = request.POST['first_name']
        new_item.last_name = request.POST['last_name']
        new_item.date_of_birth = request.POST['date_of_birth']# "YYYY-DD-MM"
        new_item.photo = file_name
        new_item.votes = 0
        new_item.save()
        message = 'ok'
        #except Exception as e:

            #message = 'error: %s' % (str(e))
        return HttpResponse(message)
    return HttpResponse(message)

def init_voting(request):
    myform = PersonsForm()
    info_list = Persons.objects.all().values('photo', 'rating', 'last_name', 'first_name','id')
    context = {'form': myform, 'persons': info_list, "MEDIA_URL": MEDIA_URL}
    return render(request, 'voting.html', context)

def set_vote(request):
    from django.db.models import F

    statusCode = 200
    message = ''
    if request.GET and 'id' in request.GET and 'value' in request.GET:
        person_id = request.GET['id']
        vote = int(request.GET['value'])
        pers = Persons.objects.get(id=person_id)
        if pers.rating + vote <= 10:
            pers.rating = F('rating') + vote
            pers.save()
        
        pers = Persons.objects.get(id=person_id)
        message = pers.rating
    else:
        statusCode = 400
        message = 'error: not enough data! Client id and vote required!'
        
    return HttpResponse(message, status=statusCode)

def calculate_age(born):
    today = datetime.date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))
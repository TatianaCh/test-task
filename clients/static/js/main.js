$(document).ready(function() {
    $('.js-download-document').click(function() {
        if (confirm('Хотите скачать?')) {
            window.open(window.location.origin + '/export');
        }
    });
    $('.js-add-client-form').submit(onAddNew);
    $('input[name="date_of_birth"]').attr('type', 'date');

    $('.toggle-modal').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();

        if ($(this).data()['modal']) {
            $($(this).data()['modal']).fadeIn();
        }
    });

    $('.js-modal-close').on('click', function(event) {
        closeModal();
    });

    $('.modal').on('click', function(event) {
        if (event.target.classList.contains('modal')) {
            closeModal();
        }
    });
});

function createPhoto(value) {
    return "<img src='" + MEDIA_DIR + value + "' alt='Photo'/>";
}

function closeModal() {
    $('.modal').fadeOut();
    $('.modal .error-field').removeClass('error-field');
    $('.modal input').val('');
}


function getCookie(name) {
    match = document.cookie.match(new RegExp(name + '=([^;]+)'));
    if (match) {
        return match[1];
    } else {
        return '';
    }
}

function onAddNew(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    var form = $('.js-add-client-form');
    var formFields = ['first_name', 'last_name', 'date_of_birth'];
    var formData = new FormData();
    var errMsg = '';
    var form_error = false;

    form.find('input').removeClass('error');

    for (var i = 0; i < formFields.length; i++) {
        if ($('input[name=' + formFields[i] + ']').length) {
            formData.append(formFields[i], $('input[name=' + formFields[i] + ']').val());
            if (formData.get(formFields[i]).length === 0) {
                form_error = true;
                $('input[name=' + formFields[i] + ']').addClass('error');
            }
        }
    }
    var timestamp = Date.parse(formData.get('date_of_birth'));
    var today = new Date();
    if (isNaN(timestamp) || timestamp >= today.getTime()) {
        form_error = true;
        $('input[name="date_of_birth"]').addClass('error');
        errMsg = "Введена неверная дата!";
    } else {
        formData.set(formFields[i], (new Date(formData.get('date_of_birth'))).toISOString().split('T')[0]); //"YYYY-DD-MM"
    }

    var file = $('input[name="photo"]')[0];
    if (file.files.length === 0) {
        errMsg += 'Выберите, пожалуйста, файл!';
        form_error = true;
        $('input[name=""]').addClass('error');
    } else {
        var fileType = file.files[0].type;
        var fileSize = file.files[0].size;

        if (fileType.search('image/') === -1 || fileType.indexOf('svg+xml') !== -1) {
            errMsg += 'Выберите, пожалуйста, графические файлы!';
            form_error = true;
            $('input[name="photo"]').addClass('error');
        }
        formData.append('photo', file.files[0]);
    }

    if (form_error == false) {
        $('.loader-modal').fadeIn();
        $.ajax({
            url: '/save/',
            method: 'POST',
            data: formData,
            dataType: 'text',
            headers: {
                'X-CSRFToken': getCookie('csrftoken')
            },
            processData: false,
            contentType: false,
            success: function(response) {
                $('.loader-modal').fadeOut();
                if (response !== 'ok') {
                    alert('Произошла ошибка: ' + response);
                    return;
                }
                alert('Новый клиент добавлен');
                var myEvent = new CustomEvent("updatecatalog");
                document.dispatchEvent(myEvent);
                closeModal();
            },
            error: function(response) {
                $('.loader-modal').fadeOut();
                alert('Произошла ошибка: ' + response.responseText);
            }
        });
    } else {
        alert(errMsg);
    }
}

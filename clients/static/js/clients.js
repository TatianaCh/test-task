$(document).ready(function() {
    setStore();

    $(document).on('updatecatalog', setStore);

    $(".js-up-arrow, .js-down-arrow").click(function(e) {
        $('.js-sorting-controls .js-up-arrow, .js-sorting-controls .js-down-arrow').removeClass('active');
        $(e.target).addClass('active');
        setStore();
    });

    $('.js-search-btn').click(function(e) {
        setStore();
    });

    $('.js-delete-btn').click(deleteSelected);
});

function collectFilterInfo() {
    var queryFilter = {};
    var arrow = $('.js-sorting-controls .active');
    if (arrow.length) {
        var orderField = arrow.closest('.js-sorting-controls');
        var sortingField = orderField.data()['field'];

        if (arrow.hasClass('js-up-arrow')) {
            sortingField = '-' + sortingField;
        }
        queryFilter['order_by'] = sortingField;
    }

    var searchText = $('.js-search-value').val();
    if (searchText.length) {
        var searchField = $('.js-search-field').val();
        queryFilter['filter_f'] = searchField;
        queryFilter['filter_v'] = searchText;
    }
    return queryFilter;
}


function setStore() {
    var queryFilter = collectFilterInfo();

    $.ajax({
        url: "getclients/",
        data: queryFilter,
        dataType: 'json',
        success: function(data) {
            if (data['client_list'].length) {
                var tableBody = $('.js-catalog-table tbody');
                tableBody.empty();
                for (var i = 0; i < data['client_list'].length; i++) {

                    var clientItem = $('<tr></tr>');
                    clientItem.append($('<td></td>').html(createPhoto(data['client_list'][i]['photo'])))
                        .append($('<td></td>').text(data['client_list'][i]['first_name']))
                        .append($('<td></td>').text(data['client_list'][i]['last_name']))
                        .append($('<td></td>').text(data['client_list'][i]['date_of_birth']))
                        .append($('<td></td>').text(data['client_list'][i]['age']))
                        .append($('<td></td>').html('<input type="checkbox" id="client-' + data['client_list'][i]['id'] + '"/>'));

                    tableBody.append(clientItem);
                }
                $('.js-catalog-table').removeClass('hidden');
                $('.js-empty-catalog').addClass('hidden');
            } else {
                $('.js-catalog-table').addClass('hidden');
                $('.js-empty-catalog').removeClass('hidden');
            }
        },
        error: function(error) {
            console.log("An unexpected error occurred: " + error);
        }
    })
}

function deleteSelected() {
    var items = $('.js-catalog-table input[type="checkbox"]:checked');
    if (items.length) {
        var ids = [];
        for (var i = 0; i < items.length; i++) {
            ids.push(items[i].id.replace('client-', ''));
        }
        $('.loader-modal').fadeIn();
        $.ajax({
            url: 'delete/',
            data: { 'ids': ids.join(',') },
            dataType: 'text',
            success: function(response, ioArgs) {
                $('.loader-modal').fadeOut();
                if (response !== 'ok') {
                    alert('Произошла ошибка: ' + response);
                    return;
                }
                setStore();
                alert('Выбранные клиенты удалены');
            },
            error: function(response, ioArgs) {
                $('.loader-modal').fadeOut();
                alert('Произошла ошибка: ' + response.responseText);
            }
        });
    }
}

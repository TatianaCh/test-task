$(document).ready(function() {
    $(document).on('updatecatalog', updateGallery);
    $('.js-voting-controls .voting-control').click(vote);

});
function updateGallery() {
	location.reload();
}

function vote(e) {
	var elem = e.target;
	var value;
	if (elem.classList.contains('down')) {
		value = -1;
	} else {
		if (elem.classList.contains('up')) {
			value = 1;
		} else {
			return;
		}
	}

	var container = $(elem).closest('.js-voting-controls');
    $.ajax({
        url: '/setvote/',
        data: {'id': container.data()['client'], value: value},
        dataType: 'text',
        success: function (response) {
            container.find('.rating').text(response);
        },
        error: function (response) {
            alert('Произошла ошибка: ' + response.responseText);
        }
    });
    
}

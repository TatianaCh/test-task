# -*- coding: utf-8 -*-
import datetime
import os


def make_clients_dict(info_list, birth_field):
    from clients.models import Persons

    field_list = {}
    client_list = []
    for row in Persons._meta.fields:
        field_list[row.name] = row.verbose_name
    for row in info_list:
        dic = {}
        for i in field_list.keys():
            if type(row[i]) == datetime.date:
                dic[i] = row[i].strftime("%d.%m.%Y")
            else:
                dic[i] = row[i]
        dic['age'] = calculate_age(row[birth_field])
        client_list.append(dic)

    field_list['age'] = 'Возраст'
    return {'client_list': client_list, 'field_list': field_list};

def calculate_age(born):
    today = datetime.date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))
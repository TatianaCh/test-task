from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from clients import views
from clientdb import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.init),
    url(r'^save/$', views.save_person),
    url(r'^delete/$', views.delete_person),
    url(r'^voting/$', views.init_voting),
    url(r'^setvote/$', views.set_vote),
    url(r'^export/$', views.export_to_xls),
    url(r'^getclients/$', views.get_clients)
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
